#!/bin/bash
set -e

export DLP_APP_NAME=$1

apk add docker-compose jq

wget -q -O /usr/bin/yq https://github.com/mikefarah/yq/releases/download/$LIB_YQ_VERSION/yq_linux_amd64 &&  chmod +x /usr/bin/yq

# Login to docker proxy registry
docker login $ARTIFACTORY_DOCKER_PROXY -u $ARTIFACTORY_USER -p $ARTIFACTORY_API_KEY

# Run clair scanning
cd $SECURITY_SCRIPTS_DIR/clair

echo ${CLAIR_WHITELIST} | yq r --prettyPrint - > whitelist.yml

docker-compose up --abort-on-container-exit --exit-code-from gitlab-org | tee output.log

cd ${CI_PROJECT_DIR}
# copy the output file from dind container to outside
docker cp clair-scanner:gl-container-scanning-report.json "$KLAR_OUTPUT_TEMPLATE"

## Check if any unapproved vulnerabilities are present, if so fail
num=$(jq '.vulnerabilities | length' "$KLAR_OUTPUT_TEMPLATE")
if [ $num != "0" ]
then
 echo "FAILED, unapproved vulnerabilities exists!!!. Please see $KLAR_OUTPUT_TEMPLATE under artifacts to view the full report"
 echo 'Total unapproved vulnerabilities : '$num
 exit 1
else
 echo "SUCCESS, no unapproved vulnerabilities exists."
 exit 0
fi


