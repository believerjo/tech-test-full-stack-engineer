#!/bin/bash
set -e

APP_DIR=$BASE_SERVICE_DIR/$1
SCANNER_IMAGE=$1-sast-scanner

apk add jq

docker rm -f $SCANNER_IMAGE || true
docker run --name $SCANNER_IMAGE --privileged --volume /var/run/docker.sock:/var/run/docker.sock  -v $APP_DIR:/code -e SAST_DEFAULT_ANALYZERS="eslint" -e SAST_ANALYZER_IMAGE_TAG="2.10.1" "registry.gitlab.com/gitlab-org/security-products/sast" /app/bin/run /code

# copy the output file from dind container to outside
docker cp $SCANNER_IMAGE:/code/gl-sast-report.json "$SAST_OUTPUT_TEMPLATE"

## Check if any unapproved vulnerabilities are present, if so fail
num=$(jq '.vulnerabilities | length' "$SAST_OUTPUT_TEMPLATE")
if [ $num != "0" ]
then
 echo "FAILED, unapproved vulnerabilities exists!!!. Please see $SAST_OUTPUT_TEMPLATE under artifacts to view the full report"
 echo 'Total unapproved vulnerabilities : '$num
 exit 1
else
 echo "SUCCESS, no unapproved vulnerabilities exists."
 exit 0
fi


