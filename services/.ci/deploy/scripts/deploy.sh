#!/bin/bash
set -e

API=$DLP_RELEASES_API
API_KEY=$DLP_RELEASES_API_TOKEN
ACTION=$1
APP=$RELEASE_NAME
COMMIT=$CI_COMMIT_SHA
NAMESPACE=$NAMESPACE
BY=$GITLAB_USER_NAME
PIPELINE_URL=$CI_PIPELINE_URL
JOB_URL=$CI_JOB_URL
SERVICE_PATH=$BASE_SERVICE_DIR/$RELEASE_NAME
VERSION_FILE_NAME=$CI_JOB_ID-version.txt
SCRIPTS_PATH=$DEPLOY_SCRIPTS_PATH
RELEASE_EXISTS=false

function create_release
{
  cd $SCRIPTS_PATH
  DATA=$(curl --location --request POST $API/create \
  --header 'Content-Type: application/json' \
  --header 'x-api-key: '$API_KEY'' \
  --data-raw '{
    "app": "'"$APP"'",
    "commitHash": "'"$COMMIT"'",
    "environment": "'"$NAMESPACE"'",
    "pipelineUrl": "'"$PIPELINE_URL"'",
    "by": "'"$BY"'"
  }')

  MS_RELEASE_NAME=$(echo $DATA | jq -r .name)
  RELEASE_EXISTS=$(echo $DATA | jq -r .exists)

  echo 'Release created for : '$MS_RELEASE_NAME
  echo $MS_RELEASE_NAME > $VERSION_FILE_NAME

  # Update the values file
  if [ -d $SERVICE_PATH/.ci/templates ]
  then
    yq w -i $SERVICE_PATH/.ci/config/$NAMESPACE/values.$NAMESPACE.yaml 'env.BUILD_NUMBER' $MS_RELEASE_NAME
  else
    yq w -i $SERVICE_PATH/.ci/$NAMESPACE/values.$NAMESPACE.yaml 'env.BUILD_NUMBER' $MS_RELEASE_NAME
  fi
}

function complete_release
{
  cd $SCRIPTS_PATH
  BUILD_NUMBER=$(cat $VERSION_FILE_NAME)
  curl --location --request POST $API/complete \
  --header 'Content-Type: application/json' \
  --header 'x-api-key: '$API_KEY'' \
  --data-raw '{
    "app": "'"$APP"'",
    "commitHash": "'"$COMMIT"'",
    "jobUrl": "'"$JOB_URL"'",
    "by": "'"$BY"'",
    "releaseName": "'"$BUILD_NUMBER"'"
  }'

  echo ''
  echo 'Release completed for : '$BUILD_NUMBER

  if [ $RELEASE_EXISTS == 'false' ]
  then
    cd $BASE_SERVICE_DIR
    echo 'Tagging release : '$BUILD_NUMBER
    git tag $BUILD_NUMBER
    git status
    git push "$CI_SERVER_PROTOCOL://oauth2:$GIT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH" --tags
  fi
}

function promote_release
{
  cd $SCRIPTS_PATH
  BUILD_NUMBER=$(cat $VERSION_FILE_NAME)
  curl --location --request POST $API/promote \
  --header 'Content-Type: application/json' \
  --header 'x-api-key: '$API_KEY'' \
  --data-raw '{
    "app": "'"$APP"'",
    "commitHash": "'"$COMMIT"'",
    "jobUrl": "'"$JOB_URL"'",
    "by": "'"$BY"'",
    "releaseName": "'"$BUILD_NUMBER"'",
    "environment": "'"$NAMESPACE"'"
  }'

  echo ''
  echo 'Promote completed for : '$BUILD_NUMBER
}

function deploy
{
    if [ $NAMESPACE == 'prod' ]
    then
        VALUE_ENV="config/values.prd.yaml"
    else
        VALUE_ENV="config/values.npe.yaml"
    fi

    if [ -d $SERVICE_PATH/.ci/templates ]
    then
        cd $SERVICE_PATH/.ci
        helm secrets upgrade --atomic --install $RELEASE_NAME --namespace=$NAMESPACE --version=$CHART_VERSION $CHART_NAME \
          -f config/$NAMESPACE/values.$NAMESPACE.yaml \
          -f config/$NAMESPACE/secrets.$NAMESPACE.yaml \
          --set image.tag=$CI_COMMIT_SHA
    else
        cd $BASE_SERVICE_DIR/.ci
        yq w -i Chart.yaml 'name' $RELEASE_NAME
        helm secrets upgrade --atomic --install $RELEASE_NAME --namespace=$NAMESPACE --version=$CHART_VERSION $CHART_NAME \
          -f config/values.yaml \
          -f $VALUE_ENV \
          -f $SERVICE_PATH/.ci/values.$RELEASE_NAME.yaml \
          -f $SERVICE_PATH/.ci/$NAMESPACE/values.$NAMESPACE.yaml \
          -f $SERVICE_PATH/.ci/$NAMESPACE/secrets.$NAMESPACE.yaml \
          --set image.tag=$CI_COMMIT_SHA
    fi
}

function diff
{
    if [ $NAMESPACE == 'prod' ]
    then
        VALUE_ENV="config/values.prd.yaml"
    else
        VALUE_ENV="config/values.npe.yaml"
    fi

    if [ -d $SERVICE_PATH/.ci/templates ]
    then
        cd $SERVICE_PATH/.ci
        helm secrets diff upgrade --allow-unreleased $RELEASE_NAME --namespace=$NAMESPACE --version=$CHART_VERSION $CHART_NAME \
          -f config/$NAMESPACE/values.$NAMESPACE.yaml \
          -f config/$NAMESPACE/secrets.$NAMESPACE.yaml \
          --set  image.tag=$CI_COMMIT_SHA
    else
        cd $BASE_SERVICE_DIR/.ci
        yq w -i Chart.yaml 'name' $RELEASE_NAME
        helm secrets diff upgrade --allow-unreleased $RELEASE_NAME --namespace=$NAMESPACE --version=$CHART_VERSION $CHART_NAME \
          -f config/values.yaml \
          -f $VALUE_ENV \
          -f $SERVICE_PATH/.ci/values.$RELEASE_NAME.yaml \
          -f $SERVICE_PATH/.ci/$NAMESPACE/values.$NAMESPACE.yaml \
          -f $SERVICE_PATH/.ci/$NAMESPACE/secrets.$NAMESPACE.yaml \
          --set image.tag=$CI_COMMIT_SHA
    fi
}

# Validations
if [ $NAMESPACE == 'prod' ]
then
    if [ $ACTION != 'diff' ] && [ $ACTION != 'promote' ]
    then
        echo 'Error - Production deployment action must be either promote or diff!!!'
        exit 1
    fi
elif [ $NAMESPACE == 'uat' ]
then
    if [ $ACTION != 'diff' ] && [ $ACTION != 'release' ]
    then
        echo 'Error - Uat deployment action must be either release or diff!!!'
        exit 1
    fi
fi

# Actions
if [ $ACTION == 'release' ]
then
    # create or get the release if already created
    create_release

    # deploy
    deploy

    # mark the release as completed
    complete_release
elif [ $ACTION == 'promote' ]
then
    # create or get the release if already created
    create_release

    # deploy
    deploy

    # mark the release as completed
    promote_release
elif [ $ACTION == 'diff' ]
then
    # run the diff
    diff
else
    # just deploy
    deploy
fi
