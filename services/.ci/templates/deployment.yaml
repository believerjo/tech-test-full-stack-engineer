apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "app.fullname" . }}
  labels:
{{ include "app.labels" . | indent 4 }}
spec:
  {{- if .Values.hpa }}
  {{- else }}
  replicas: {{  .Values.replicaCount }}
  {{- end }}
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "app.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
      developer: {{ .Values.developer }}
  template:
    metadata:
      annotations:
        timestamp: "{{ .Values.timestamp }}"
      labels:
        app.kubernetes.io/name: {{ include "app.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        developer: {{ .Values.developer }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      volumes:
        {{ include "volumes.config" . | nindent 8 }}
        - name: fluentbit-config
          configMap:
            name: {{ include "app.fullname" . }}-env
            items:
              - key: FILTER_CONF
                path: filter.conf
              - key: FLUENTBIT_CONF
                path: fluent-bit.conf
              - key: SERVICE_CONF
                path: service.conf
              - key: OUTPUT_CONF
                path: output.conf
              - key: PARSERS_CONF
                path: parsers.conf
              - key: INPUT_CONF
                path: input.conf
        - name: varlibdockercontainers
          hostPath:
            path: /var/lib/docker/containers
        - name: varlog
          hostPath:
            path: /var/log
      containers:
        - name: {{ include "app.fullname" . }}
          image: "{{ .Values.image.repository }}/{{ .Values.nameOverride }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          securityContext:
            runAsUser: 1000
          volumeMounts:
          {{ include "volumes.mount.config" . | nindent 13 }}
          env:
            - name: NODE_ENV
              value: {{ .Values.namespace }}
            - name: SERVICE_NAME
              value: {{ .Values.nameOverride}}
            - name: NEW_RELIC_APP_NAME
              value: "{{ .Values.namespace }}.gle-{{ .Values.nameOverride}}"
          envFrom:
            - secretRef:
                name: {{ include "app.fullname" . }}-secret
            - configMapRef:
                name: {{ include "app.fullname" . }}-env
          ports:
            - name: http
              containerPort: {{ .Values.env.PORT }}
              protocol: TCP
          resources:
            {{- toYaml .Values.resources.app | nindent 12 }}
            {{- if .Values.livenessProbe.enabled }}
          livenessProbe:
            initialDelaySeconds: {{ .Values.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.livenessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.livenessProbe.successThreshold }}
            failureThreshold: {{ .Values.livenessProbe.failureThreshold }}
            httpGet:
              path: /health-check
              port: {{ .Values.env.PORT }}
              {{- end }}
            {{- if .Values.readinessProbe.enabled }}
          readinessProbe:
            initialDelaySeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.readinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.readinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.readinessProbe.successThreshold }}
            failureThreshold: {{ .Values.readinessProbe.failureThreshold }}
            httpGet:
              path: /health-check
              port: {{ .Values.env.PORT }}
              {{- end }}  
        - name : sidecar
          image: {{ .Values.image.fluentbit }}
          env:
          - name: SPLUNK_HEC_HOST
            value: {{ .Values.env.SPLUNK_HEC_HOST}}
          - name: SPLUNK_HEC_TOKEN
            value: {{ .Values.env.SPLUNK_HEC_TOKEN}}
          - name: SPLUNK_INDEX
            value: {{ .Values.env.SPLUNK_INDEX}}
          - name: NAMESPACE
            value: {{ .Values.namespace }}
          - name: APP_NAME
            value: {{ include "app.name" . }}
          - name: MY_POD_NAME
            value: {{ include "app.name" . }}-{{ .Values.namespace }}
          - name: SPLUNK_START_ARGS
            value: {{ .Values.env.SPLUNK_START_ARGS}}
          volumeMounts:
            - name: varlibdockercontainers
              mountPath: /var/lib/docker/containers
            - name: fluentbit-config
              mountPath: /fluent-bit/etc
            - name: varlog
              mountPath: /var/log
          resources:
            {{- toYaml .Values.resources.sidecar | nindent 12 }}
      {{- if eq .Values.namespace "load" }}
      nodeSelector:
        nodeType: "load"
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- if eq .Values.namespace "load" }}
      tolerations:
        - key: "dedicated"
          value: "LOAD"
          operator: "Equal"
          effect: "NoSchedule"
    {{- end }}
