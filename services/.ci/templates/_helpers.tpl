{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "app.name" -}}
{{- default .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "app.labels" -}}
app.kubernetes.io/name: {{ include "app.name" . }}
helm.sh/chart: {{ include "app.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "ingress.annotations" -}}
alb.ingress.kubernetes.io/healthcheck-path: /health-check
alb.ingress.kubernetes.io/ssl-policy: {{ .Values.ingress.sslPolicy }}
kubernetes.io/ingress.class: alb
alb.ingress.kubernetes.io/listen-ports: {{ .Values.ingress.listenPorts }}
alb.ingress.kubernetes.io/certificate-arn: {{ .Values.ingress.certificate }}
alb.ingress.kubernetes.io/load-balancer-attributes: access_logs.s3.enabled=true,access_logs.s3.bucket={{ .Values.ingress.s3logsBucket }},access_logs.s3.prefix=gle-{{ .Values.nameOverride }}-{{ .Values.namespace }}
{{- if eq .Values.ingress.type "private" }}
{{- if .Values.ingress.private.waf }}
alb.ingress.kubernetes.io/web-acl-id: {{ .Values.ingress.private.waf }}
{{- end }}
alb.ingress.kubernetes.io/security-groups: {{ .Values.ingress.private.securityGroup }}
alb.ingress.kubernetes.io/scheme: {{ .Values.ingress.private.scheme }}
{{- if eq .Values.namespace "prod" }}
external-dns.alpha.kubernetes.io/hostname: gle-{{ .Values.nameOverride }}.{{ .Values.ingress.private.domain }}
{{- else }}
external-dns.alpha.kubernetes.io/hostname: gle-{{ .Values.nameOverride }}-{{ .Values.namespace }}.{{ .Values.ingress.private.domain }}
{{- end }}
{{- end }}
{{- if eq .Values.ingress.type "public" }}
alb.ingress.kubernetes.io/security-groups: {{ .Values.ingress.public.securityGroup }}
alb.ingress.kubernetes.io/scheme: {{ .Values.ingress.public.scheme }}
{{- if eq .Values.namespace "prod" }}
external-dns.alpha.kubernetes.io/hostname: gle-{{ .Values.nameOverride }}.{{ .Values.ingress.public.domain }}
{{- else }}
external-dns.alpha.kubernetes.io/hostname: gle-{{ .Values.nameOverride }}-{{ .Values.namespace }}.{{ .Values.ingress.public.domain }}
{{- end }}
{{- end }}
{{- end -}}

{{- define "volumes.config" -}}
{{- $appName := .Values.fullnameOverride }}
{{- range $mount := .Values.mounts }}
- name: {{ $mount.name }}
  secret:
    secretName: {{ $appName }}-secret
    items:
      - key: {{ $mount.key }}
        path: {{ $mount.path }}
{{- end }}
{{- end -}}

{{- define "volumes.mount.config" -}}
{{- range $mount := .Values.mounts }}
- name: {{ $mount.name }}
  mountPath: {{ $mount.mountPath }}
  subPath: {{ $mount.path }}
  readOnly: {{ $mount.readOnly }}
{{- end }}
{{- end -}}
