#!/bin/bash
set -e

SERVICE_PATH=$1
APP_NAME=$2
DOCKER_IMAGE=$DOCKER_REGISTRY/$APP_NAME:$CI_COMMIT_SHA

# Build and test
docker build --network host -t "$DOCKER_IMAGE" --build-arg SERVICE_PATH="$SERVICE_PATH" $SERVICE_PATH/.

# Push
docker push $DOCKER_IMAGE
