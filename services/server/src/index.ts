import * as express from 'express';

const server = express();
const port = process.env.PORT;

server.listen(port, () => {
    console.log(`Server is listening at http://localhost:${port}`);
});
